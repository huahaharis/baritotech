/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {Text, TouchableOpacity, View, Image} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import Home from './src/screens/index';
import AddNewTodo from './src/screens/AddNewTodo';
import {NativeBaseProvider, AddIcon, Avatar} from 'native-base';
import axios from 'axios';


const Stack = createStackNavigator();

function App() {
  const [user, setUser] = React.useState('');

  React.useEffect(() => {
    axios.get('https://random-data-api.com/api/users/random_user')
    .then((response)=>{
      setUser(response.data)
    })
    .catch((err)=>{
      console.log(err);
    })

  },[]);

  return (
    <NativeBaseProvider>
      <NavigationContainer>
        <Stack.Navigator>
          <Stack.Screen
            name="Home"
            component={Home}
            options={({route, navigation}) => ({
              headerLeft: () => (
                <View>
                  <Image
                    source={{uri: user.avatar}}
                    style={{
                      width: 30,
                      height: 30,
                      backgroundColor: '#c5d3e8',
                      alignItems: 'center',
                      justifyContent: 'center',
                      left: 20,
                      borderRadius: 20
                    }}
                  />
                </View>
              ),
              headerTitle: <Text>Todo List</Text>,
              headerRight: () => (
                <TouchableOpacity
                  style={{right: 20}}
                  onPress={() => navigation.navigate('AddNewTodo')}>
                  <AddIcon size="4" />
                </TouchableOpacity>
              ),
            })}
          />
          <Stack.Screen
            name="AddNewTodo"
            component={AddNewTodo}
            options={{
              headerTitle: <Text>Add New Todo List</Text>,
            }}
          />
        </Stack.Navigator>
      </NavigationContainer>
    </NativeBaseProvider>
  );
}

export default App;
