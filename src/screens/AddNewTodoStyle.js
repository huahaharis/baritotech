import { StyleSheet } from 'react-native'

const Styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  textInput: {
    width: '85%',
    height: '5%',
    backgroundColor: '#d7d9d7',
    borderRadius: 10,
    paddingLeft: 4,
    alignSelf: 'center',
    shadowColor: '#000',
    shadowOffset: {width: 0, height: 2},
    shadowOpacity: 0.8,
    shadowRadius: 1,
  },
  tagTitle: {
    fontSize: 16,
    fontWeight: 'bold',
  },
  warperTagTitle: {
    marginTop: '4%',
    marginLeft: '8%',
  },
  textArea: {
    width: '85%',
    height: '25%',
    backgroundColor: '#d7d9d7',
    borderRadius: 10,
    paddingLeft: 4,
    alignSelf: 'center',
    left: 2,
    shadowColor: '#000',
    shadowOffset: {width: 0, height: 2},
    shadowOpacity: 0.8,
    shadowRadius: 1,
  },
});

export default Styles