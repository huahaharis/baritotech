import { StyleSheet } from 'react-native'

const Styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  buttonEdit: {
    bottom: 5,
    width: 60,
    height: 20,
    backgroundColor: '#FFF',
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonDelete: {
    top: 5,
    width: 60,
    height: 20,
    backgroundColor: '#FFF',
    justifyContent: 'center',
    alignItems: 'center',
  },
  containerSearch: {
    flexDirection: 'row',
    backgroundColor: 'grey',
    width: '67%',
    justifyContent: 'center',
    alignSelf: 'center',
    marginVertical: 5,
    borderRadius: 10,
  },
});

export default Styles