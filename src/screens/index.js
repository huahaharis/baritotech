import React from 'react'
import {SafeAreaView, View, Text, TouchableOpacity, TextInput} from 'react-native';
import Styles from './IndexStyle';
import Loading from '../components/loading';
import DraggableFlatList from 'react-native-draggable-flatlist';
import {useSelector, useDispatch} from 'react-redux'
import {useNavigation} from '@react-navigation/native'
import {SearchIcon, Popover, Button} from 'native-base'
import {ADDTODO_DELETE} from '../redux/actions'


const NUM_ITEMS = 10;
function getColor(number){
  const multiplier = 255 / (NUM_ITEMS - 1);
  const colorVal = number * multiplier;
  return `rgb(${colorVal}, ${Math.abs(128 - colorVal)}, ${255 - colorVal})`;
}

const Home = () => {
    const navigation = useNavigation();
    const dispatch = useDispatch();
    const [loading, setLoading] = React.useState(false)
    const [crossed, setCrossed] = React.useState(false)
    const [search, setSearch] = React.useState('')
    const [dataSearch, setDataSearch] = React.useState([])
    const [data, setData] = React.useState([])
    const globalState = useSelector((state) => state);


    React.useEffect(()=>{
        setData(globalState.TodolistReducers.todolist);
        if(search.length == 0 ){
          setDataSearch([])
        }
    },[globalState || search ])


    const searchData = () => {
      //Asdhasdvf
      let findData1 = data.filter(data => data.title == search)
      let findData2 = data.filter(data => data.category == search)
      let findData3 = data.filter(data => data.status == search)
      console.log(findData1, findData2, findData3);
      if(findData1.length > 0){
        setDataSearch(findData1)
      } else if( findData2.length > 0){
        setDataSearch(findData2)
      } else {
        setDataSearch(findData3)
      }
    }
    
    const doneTodo = (item, index) => {
      if (item.crossed == 1) {
        let array = data;
        array[index].crossed = 0;
        setData(data);
        setCrossed(false)
      } else {
        let array = data;
        array[index].crossed = 1;
        setData(data);
        setCrossed(true)
      }
    };


    const deleteData = (id)=>{
      let dlt = [...data]
      let value = dlt.findIndex(x => x.id == id)
      if(value > -1){
        dlt.splice(value, 1)
        setData(dlt)
      }
    }

    const renderItem = ({item, index, drag, isActive}) => {
      let background = getColor(index);
      return (
        <TouchableOpacity
          key={index}
          style={{
            height: 100,
            marginVertical: 4,
            backgroundColor: isActive ? 'red' : background,
            alignItems: 'center',
            justifyContent: 'center',
            borderRadius: 20
          }}
          onPress={()=> doneTodo(item, index)}
          onLongPress={drag}>
          <View style={{flexDirection: 'row', paddingHorizontal: 80}}>
            {item.crossed == 1 ? (
            <Text
              style={{
                fontWeight: 'bold',
                color: 'white',
                fontSize: 26,
                textAlign: 'center',
                flexWrap: 'wrap',
                flexShrink: 1,
                textDecorationLine: 'line-through', 
                textDecorationStyle: 'solid',
                textDecorationColor: 'black'
              }}
              numberOfLines={2}>
              {item?.title}
            </Text>
            ):(
            <Text
              style={{
                fontWeight: 'bold',
                color: 'white',
                fontSize: 26,
                textAlign: 'center',
                flexWrap: 'wrap',
                flexShrink: 1,
              }}
              numberOfLines={2}>
              {item?.title}
            </Text>)}
            <View
              style={{
                flexDirection: 'column',
                alignContent: 'center',
                justifyContent: 'center',
                alignSelf: 'center',
                left: item.title.length > 4 ? item.title.length > 5 ? item.title.length > 10 ? 30 : 50 : 69 : 85,
              }}>
              <Popover
                trigger={triggerProps => {
                  return (
                    <TouchableOpacity
                      {...triggerProps}
                      style={Styles.buttonEdit}>
                      <Text>Delete</Text>
                    </TouchableOpacity>
                  );
                }}>
                <Popover.Content>
                  <Popover.Arrow />
                  <Popover.CloseButton />
                  <Popover.Header>Delete Data</Popover.Header>
                  <Popover.Body>
                    This will remove this data from list, are you sure with that?
                  </Popover.Body>
                  <Popover.Footer justifyContent="flex-end">
                    <Button.Group space={2}>
                      <Button colorScheme="danger" onPress={()=>deleteData(item.id)}>Delete</Button>
                    </Button.Group>
                  </Popover.Footer>
                </Popover.Content>
              </Popover>
              <TouchableOpacity style={Styles.buttonDelete}>
                <Text>Edit</Text>
              </TouchableOpacity>
            </View>
          </View>
        </TouchableOpacity>
      );
    }


    return (
      <SafeAreaView style={Styles.container}>
        <Loading visible={loading} />
        <View style={Styles.containerSearch}>
          <TextInput
            value={search}
            style={{width: '80%'}}
            onChangeText={value => setSearch(value)}
          />
          <TouchableOpacity onPress={() => searchData()}>
            <SearchIcon style={{width: 30, height: 30, left: 6}} />
          </TouchableOpacity>
        </View>
        <View style={{flex: 1}}>
          {search !== '' ? (
            <DraggableFlatList
              data={dataSearch}
              renderItem={renderItem}
              keyExtractor={(item, index) => index.toString()}
              onDragEnd={({data}) => setData(data)}
            />
          ) : (
            <DraggableFlatList
              data={data}
              renderItem={renderItem}
              keyExtractor={(item, index) => index.toString()}
              onDragEnd={({data}) => setData(data)}
            />
          )}
        </View>
      </SafeAreaView>
    );
}

export default Home