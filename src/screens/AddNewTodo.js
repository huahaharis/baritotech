import React from 'react';
import {SafeAreaView, View, Text, TextInput} from 'react-native';
import {TextArea} from 'native-base';
import Styles from './AddNewTodoStyle';
import Button from '../components/button';
import {useDispatch} from 'react-redux';
import {ADDTODO} from '../redux/actions';
import Loading from '../components/loading';
import {useNavigation} from '@react-navigation/native'

const AddNewTodo = (props) => {
  const dispatch = useDispatch();
  const navigation = useNavigation();
  const [form, setForm] = React.useState({
    id: '',
    title: '',
    status: '',
    category: '',
    description: '',
    crossed: 0
  });
  const [loading, setLoading] = React.useState(false);


  React.useEffect(() => {
    if (props.route.params?.data != undefined) {
      setForm({
        id: props.route.params.data.id,
        title: props.route.params.data.title,
        category: props.route.params.data.category,
        status: props.route.params.data.status,
        description: props.route.params.data.description,
      });
    }
  }, [props.route.params?.data]);

  const Uuid = (length) => {
    const result = [];
    const chars = `ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789`;
    const charLength = chars.length;
  
    function getRandomInt(min, max) {
      return Math.floor(Math.random() * (max - min + 1)) + min;
    }
  
    for (let i = 0; i < length; ++i) {
      result.push(chars[getRandomInt(0, charLength - 1)]);
    }
  
    return result.join(``);
  };

  const onInputChange = (value, input) => {
    setForm({
      ...form,
      id: Uuid(30).toString(),
      [input]: value,
    });
  };


  const inputToRedux = async () => {
    setLoading(true);
    await setTimeout(() => {
      setLoading(false);
      dispatch({
        type: ADDTODO,
        payload: form,
      });
      setForm({
        id: '',
        title: '',
        status: '',
        category: '',
        description: '',
      });
    }, 5000);
    navigation.navigate('Home')

    return ()=>clearTimeout(5000)
  };

  return (
    <SafeAreaView style={Styles.container}>
      <Loading visible={loading} />
      <View style={Styles.warperTagTitle}>
        <Text style={Styles.tagTitle}>Name Activity</Text>
      </View>
      <TextInput
        value={form.title}
        placeholder={'Input your activity name'}
        style={Styles.textInput}
        onChangeText={value => onInputChange(value, 'title')}
      />
      <View style={Styles.warperTagTitle}>
        <Text style={Styles.tagTitle}>Status</Text>
      </View>
      <TextInput
        value={form.status}
        placeholder={'Input your status activity'}
        style={Styles.textInput}
        onChangeText={value => onInputChange(value, 'status')}
      />
      <View style={Styles.warperTagTitle}>
        <Text style={Styles.tagTitle}>Category</Text>
      </View>
      <TextInput
        value={form.category}
        placeholder={'Input your activity category'}
        style={Styles.textInput}
        onChangeText={value => onInputChange(value, 'category')}
      />
      <View style={Styles.warperTagTitle}>
        <Text style={Styles.tagTitle}>Description</Text>
      </View>
      <TextArea
        value={form.description}
        placeholder={'Input your activity description'}
        style={Styles.textArea}
        onChangeText={value => onInputChange(value, 'description')}
      />
      <View style={{marginTop: '10%', alignItems: 'center'}}>
        <Button data={form} onPress={inputToRedux} />
      </View>
    </SafeAreaView>
  );
};

export default AddNewTodo;
