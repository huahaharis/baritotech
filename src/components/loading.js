import * as React from 'react';
import {View, Dimensions, Modal, ActivityIndicator, Text} from 'react-native';

const {height, width} = Dimensions.get('screen')


const Loading = (props) => {
    
    return (
      <Modal
        onRequestClose={() => null}
        visible={props.visible}
        style={{backgroundColor: 'rgba(52, 52, 52, 0.8)'}}>
        <View
          style={{
            flex: 1,
            backgroundColor: 'rgba(52, 52, 52, 0.8)',
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          <ActivityIndicator size="large" />
          {/* <LottieView ss/> */}
        </View>
      </Modal>
    );
}

export default Loading;