import React from 'react'
import {TouchableOpacity, Text, StyleSheet} from 'react-native'

const Button = (props) => {
    return (
      <TouchableOpacity
        style={Styles.container}
        onPress={() => props.onPress()}>
        <Text style={{alignSelf: 'center', top: 7}}>Submit</Text>
      </TouchableOpacity>
    );
}

export default Button

const Styles = StyleSheet.create({
  container: {
    backgroundColor: 'lightblue',
    width: '85%',
    height: '22%',
    borderRadius: 10,
    shadowColor: '#000',
    shadowOffset: {width: 0, height: 2},
    shadowOpacity: 0.8,
    shadowRadius: 1,
    borderColor: 'black',
    borderWidth: 1
  },
});