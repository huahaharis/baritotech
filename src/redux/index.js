import {combineReducers} from 'redux'
import {ToDoListData as TodolistReducers} from './reducers';

export default combineReducers({
  TodolistReducers,
});
