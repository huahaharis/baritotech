import {takeEvery,put, all} from 'redux-saga/effects';
import {ADDTODO} from './actions';

function* handler() {
  yield takeEvery(ADDTODO, todoListData);
}

function* todoListData(action) {
  try {
    yield all({
        type: ADDTODO,
        payload: action.payload
    })
  } catch (error) {
  }
}
export {handler};
